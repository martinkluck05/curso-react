// Dependencies
import express from 'express';
import webpack from 'webpack';
import path from 'path';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import open from 'open';
import exphbs from 'express-handlebars';

// Config
import config from '../config';

// Webpack configuration
import webpackConfig from '../../webpack.config.babel';

// API
import blogApi from './api/blog';
import libraryApi from './api/library';

// Helpers
import * as hbsHelper from '../lib/handlebars';

// Utils
import {isMobile} from '../lib/utils/device';

const isDevelopment = process.env.NODE_ENV !== 'production';

// Express app
const app = express();

// Public
app.use(express.static(path.join(__dirname,'../public')));

// Handlebars setup
app.engine(config.views.engine,exphbs({
    extname: config.views.extension,
    helpers: hbsHelper
}));

// View Engine Setup
app.set('views',path.join(__dirname, config.views.path));
app.set('view engine','.hbs');

// Webpack compiler
const webpackCompiler = webpack(webpackConfig);

if(isDevelopment) {
    // Webpack middleware
    app.use(webpackDevMiddleware(webpackCompiler));
    app.use(webpackHotMiddleware(webpackCompiler));
}

// Device detector
app.use((req,res,next)=>{
    res.locals.isMobile = isMobile(req.header('user-agent'));
    return next();
});

// Api dispatch
app.use('/api/blog', blogApi);
app.use('/api/library', libraryApi);

// Sending all traffic to React
app.get('*',(req,res)=>{
    // res.sendFile(path.join(__dirname,'../public/index.hbs'));
    res.render('frontent/index',{
        layout: false
    });
});

// Listen Port
app.listen(config.serverPort,err=>{
    if(!err){
        open(`${config.baseUrl}`);
    }
});