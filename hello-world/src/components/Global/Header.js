// Dependencies
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
// Assets
import logo from './images/logo.svg';
import './css/Header.css';
//

class Header extends Component {
    static propTypes = {
        title: PropTypes.string.isRequired,
        items: PropTypes.array.isRequired
    };
    render() {
        const {title,items} = this.props;
        console.log(items);
        return (
            <div className="Header">
                <header className="Logo">
                    <img src={logo} alt="logo" />
                    <h1>{title}</h1>
                </header>
                <nav className="navbar navbar-expand-lg navbar-light bg-light">
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav mr-auto">
                            {
                                items && items.map(
                                    (item, key)=><li key={key} className="nav-item active"><Link className="nav-link" to={item.url}>{item.title} <span className="sr-only">(current)</span></Link></li>
                                )
                            }
                        </ul>
                    </div>
                </nav>
            </div>
        );
    }
}

export default Header;
