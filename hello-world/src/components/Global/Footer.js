import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './css/Footer.css';

class Footer extends Component {
    static propTypes = {
        copyright: PropTypes.string
    };
  render() {
      const {copyright='&copy; React 2018'} = this.props;
    return (
      <div className="Footer">
          <footer>
              <p dangerouslySetInnerHTML={{__html:copyright}}/>
          </footer>
      </div>
    );
  }
}

export default Footer;
