export const API = Object.freeze({
    LIBRARY: {
        BOOKS: 'library/books',
        BOOK: 'library/book'
    }
});